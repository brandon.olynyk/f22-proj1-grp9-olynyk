`use strict`
/**
 * Returns an object with the project information given through the parameters obtained through each input value.
 * @author Tony Nguyen
 */
function createProjectObject(i_proj_id, i_owner_name, i_title, i_category, i_hours, i_rate, i_status, i_proj_desc) {
    let proj_obj = {
        proj_id: i_proj_id,
        owner_name: i_owner_name,
        title: i_title,
        category: i_category,
        hours: i_hours,
        rate: i_rate,
        status: i_status,
        proj_desc: i_proj_desc
    }
    return proj_obj;
}

/**
 * Uses the createProjectObject function to create an object based on the input field's current values.
 * @author Brandon P. Olynyk.
 */
function createProjectObjectFromForm() {
    return createProjectObject(e_proj_id.value, e_owner_name.value, e_title.value, e_category.value, e_hours.value, e_rate.value, e_status.value, e_proj_desc.value);
}

/**
 * Adds the given project object to the project array and updates the table.
 * @author Brandon P. Olynyk & Tony Nguyen
 */
function updateProjectsTable() {
    let proj_obj = createProjectObjectFromForm();
    project_array.push(proj_obj);
    //update table
    createTableFromArrayObjects(project_array, document.querySelector("table-render-space"));
}

/**
 * Removes the project in project_array at the index. That index is left as undefined (for now).
 * The index is gotten through the event's tag name. 
 * @author Brandon P. Olynyk & Tony Nguyen
 */
function deleteTableRow() {
    let index_of_object_to_remove = event.target.id.replace("bin-img-", "");
    if (window.confirm("Are you sure you want to remove Project " + index_of_object_to_remove + "?")) {
        project_array.splice(index_of_object_to_remove, 1);  //remove entry from array 
        //update table
        createTableFromArrayObjects(project_array, document.querySelector("table-render-space"));
    }
}
/**
 * Changes all the table row's text fields to input fields and changes the row to red.
 * Changes the write icon to a save icon, as well as change its event respectively.
 * @author Brandon P. Olynyk
 */
function editTableRow() {
    //get the row and change its text to input fields
    //they're all text fields in the example doc, so they will all be text fields here
    //(even though there are dropdown options but sure)
    console.log(event.target.id);
    let row_number = parseInt(event.target.id.replace("edit-img-", ""));
    if (document.querySelector("#edit-img-" + row_number).src.endsWith("edit.png")) {

        //get all elements of the row
        let row_cells = document.querySelectorAll(".table-cell-in-row-" + row_number);

        //highlight all items and change to textfields       
        row_cells.forEach(function (element) {
            //makes a textfield, sets its value to the element's value, then replaces.  
            element.style.background = "red";
            let prev_val = element.textContent;
            element.innerHTML = "<input value=\"" + prev_val + "\" class=\"edit-input\" required=\"true\"></input>";
        })

        //change edit button to save button and change event
        document.querySelector("#edit-img-" + row_number).src = "../images/floppy.png";
        event.target.removeEventListener("mousedown", editTableRow);
        event.target.addEventListener("mouseup", saveTableRow);
    }

}

/**
 * After editing a row, saves that edited row to the project_array array, and redraw the table.
 * @author Brandon P. Olynyk
 */
function saveTableRow() {

    let row_number = parseInt(event.target.id.replace("edit-img-", ""));
    //get all elements of the row
    let row_cells = document.querySelectorAll(".table-cell-in-row-" + row_number);

    //save new obj to proj_array and redraw table
    let saved_project_values = new Array(8);
    for (let i = 0; i < saved_project_values.length; i++) {
        saved_project_values[i] = row_cells[i].childNodes[0].value;
    }
    //make the saved_project_values into an obj and set that in project_array
    project_array[row_number] = createProjectObject(saved_project_values[0], saved_project_values[1], saved_project_values[2], saved_project_values[3], saved_project_values[4], saved_project_values[5], saved_project_values[6], saved_project_values[7]);

    //create an obj with all the values got and place that as an object
    createTableFromArrayObjects(project_array, document.querySelector("table-render-space"));

}

/**
 * Saves all objects from the project_array into local storage, replacing all previous values.
 * Their keys will be stored as project-<index>.
 * @author Brandon P. Olynyk.
 */
function saveAllProjectsToStorage() {

    if (window.confirm("Are you sure you want to save? This will clear all your previous project data!")) {
        localStorage.clear();
        //foreach object save each key and value into localstorage  
        let i = 0;
        //project_array.forEach(element => localStorage.setItem("project-" + i, JSON.stringify(element)));
        project_array.forEach(function (element) {
            localStorage.setItem("project-" + i, JSON.stringify(element));
            i++;
        });

        //finally saves # of saved projects
        localStorage.setItem("saved-projects", project_array.length);
        document.getElementById("search-message").textContent = "Saved " + project_array.length + " entries into storage.";
    }
}
/**
 * Saves all objects from the project_array into local storage, keeping all previous values and appending them to the end.
 * Their keys will be stored as project-<index>.
 * @author Brandon P. Olynyk.
 */
function appendAllProjectsToStorage() {
    //there's something wrong, the appended objs are null! fix pls

    if (window.confirm("Are you sure you want to save? This will append your current data your previous project data.")) {
        let current_length = parseInt(localStorage.getItem("saved-projects"));
        let i = 0;
        //foreach object save each key and value into localstorage  
        project_array.forEach(function (element) {
            localStorage.setItem("project-" + (i + parseInt(current_length)), JSON.stringify(element));
            i++;
        });
        //finally saves # of saved projects
        localStorage.setItem("saved-projects", project_array.length + current_length);
    }
}
/**
 * Asks the user if they want to clear the local storage. If so, clear it.
 * @author Brandon P. Olynyk.
 */
function clearAllProjectsToStorage() {

    if (window.confirm("Are you sure you want to clear all your project data?")) {
        localStorage.clear();
        document.getElementById("search-message").textContent = "Storage Cleared!";
    }
}

/**
 * Reads all the saved values and puts them into project_array.
 * @author Brandon P. Olynyk.
 */
function readAllProjectsToStorage() {
    //resets project_array and loads all saved objects and puts them in project array.
    //Then redraw table.
    if (window.confirm("Are you sure you want to load all your project data? This will erase all unsaved progress!")) {
        let saved_length = localStorage.getItem("saved-projects");

        project_array = new Array();
        for (let i = 0; i < saved_length; i++) {
            project_array.push(JSON.parse(localStorage.getItem("project-" + i)));
        }
    }

    //update table and message box
    document.getElementById("search-message").textContent = "There are " + project_array.length + " projects in local storage.";
    createTableFromArrayObjects(project_array, document.querySelector("table-render-space"));
}


/** 
* First changes the type of sort by +1 (the index being the same as the project_array values index)
* Then, sorts all the project_array entries. 
* @author Brandon P. Olynyk
*/
function sortProjects() {
    try {
        //sorts all the projects, makes that the project array the sorted one.

        //increments sort_type, and clamping between 0-7
        sort_type += 1;
        if (sort_type >= 8) {
            sort_type = 0
        }

        let key_to_check = sort_type;

        //set the button to the apropriate key name
        document.querySelector("#sort-type-button").textContent = "Sort by: " + Object.entries(project_array[0])[key_to_check][0] + ", (Low to High, A-Z)";
        console.log("sorting by " + Object.entries(project_array[0])[key_to_check][0]);

        let sorting = true;

        while (sorting) {
            let one_swap_done = false; //set to true if a swap has done, for escaping the loop purposes

            project_array.forEach(function (current_obj, index) {
                let prev_obj = project_array[index - 1];


                if (prev_obj != undefined) {
                    //compares string values of the needed key name
                    let prev_val = Object.entries(prev_obj)[key_to_check][1];
                    let current_val = Object.entries(current_obj)[key_to_check][1];
                    //if string then concat to its first letter
                    if (current_val < prev_val && current_val != prev_val) {
                        //swap 'em
                        let temp = project_array[index - 1];
                        project_array[index - 1] = project_array[index];
                        project_array[index] = temp;
                        one_swap_done = true;
                    }
                }
            })
            //if no swaps were made, the sort is done. escape loop.
            if (!one_swap_done) { sorting = false; break; }
        }
        console.log("Sorting complete!!")
        //refresh table
        createTableFromArrayObjects(project_array, document.querySelector("table-render-space"));
    }
    catch (err) {
        console.warn("Tried to sort an empty array! Do nothing: " + err)
    }
}

/**
 * Creates a new object[], populates it with objects obtained by searching and filtering from project_array, and
 * sets that new array to be rendered.
 * @author Brandon P. Olynyk & Tony Nguyen
 */
function filterProjects() {

    let query_string = event.target.value;//gets query string from the input that triggered this event
    if (query_string == "") {
        //if empty just draw all projects
        createTableFromArrayObjects(project_array, document.querySelector("table-render-space"))
    }
    else {
        //draw filtered objects

        //create new array, copy of project_array
        let newArray = new Array(project_array.length);
        project_array.forEach((element, index) => { newArray[index] = element });

        newArray = newArray.filter(function (element) {
            let isValid = false;
            Object.values(element).forEach(val => isValid = val.toLowerCase().includes(query_string.toLowerCase()) || (isValid));
            console.log(isValid);
            return isValid;

            //if encountered bad value break and return false;
        })

        createTableFromArrayObjects(newArray, document.querySelector("table-render-space"));
        document.getElementById("search-message").textContent = "Found " + newArray.length + " entries for your querry.";

    }


}