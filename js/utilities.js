'use strict'
/**
 * Changes the disabled state of the button that's passed through. By Brandon P. Olynyk
 * @param cssObject The button css Object to change its disabled value
 * @param isDisabled The value to change the disabled value to
 * @author Brandon P. Olynyk
 */
function enable_disable_Button(cssObject, isDisabled) {
    cssObject.disabled = isDisabled;
}
/**
 * Changes the cssObject's required value to the specified value, either true or false.
 * @param cssObject The cssSelector id to have its required value changed.
 * @param requiredState The state to be changed to.
 * @author Brandon P. Olynyk
 */
function set_elem_required(cssID, requiredState) {
    let object = document.getElementById(cssID);
    object.required = requiredState;
    document.getElementById(cssID).parentNode.children[0].style.fontWeight = "bold";

}
/**
 * Creates the tag_object element and appends it to parent.
 * @param tag_object The tag_object element to create
 * @param parent The parent to append the created object to.
 * @author Tony Nguyen
 */
function createTagElement(tag_object, parent) {
    parent.appendChild(tag_object);
}
/**
 * Creates the tag_object element and appends it to parent.
 * @param elem_id The div with the input field and image (only one)
 * @param feedback_text The feedback text to display to the user once the error happens
 * @param error_success The bool value on whether it's an error or not. True if it's an error, false if no error happens
 * @author Brandon P. Olynyk
 */
function setElementFeedback(elem_id, feedback_text, error_success) {
    let errorCheckmark = document.getElementById(elem_id + "-valid");
    let error_message_element = document.getElementById(elem_id + "-error");

    if (error_success) {
        //if the error happened, display the error + the feedback_text
        error_message_element.style.display = "";
        error_message_element.textContent = feedback_text;
        errorCheckmark.src = "../images/x.gif";
        //image.src = "images/polarbear.jpg"
    }
    else {
        //remove the error message with display: none
        errorCheckmark.src = "../images/checkmark.gif";
        error_message_element.style.display = "none";
    }

    checkAllValidation();
}


/** 
 * Hides the feedback elements (the error messages) of all the input fields
 * @author Brandon P. Olynyk
 */
function hideElementFeedback() {

    setElementFeedback("proj-id", "", false);
    setElementFeedback("owner-name", "", false);
    setElementFeedback("title", "", false);
    setElementFeedback("hours", "", false);
    setElementFeedback("rate", "", false);
    setElementFeedback("proj-desc", "", false);
}

/**
 * Validates the regex based on the regex. elem_id is gotten automatically if run by an event, else you must supply an id in passthrough.
 * @param passthrough An optional id name to pass through if you want to validate that id, without the #.
 * @author Brandon P. Olynyk
 */
function validateElement(passthrough) {
    //gets elem id, validates based on regex and set add button and check mark
    let valid = false;  //set to true if element_id is valid
    let elem_id = event.target.id;
    if (elem_id == undefined) {
        elem_id = passthrough
    }


    switch (elem_id) {

        case "proj-id":
            //check for id string and length

            if (document.getElementById(elem_id).value.replace(proj_id_regex, "").length <= 0) {
                if (document.getElementById(elem_id).value.length >= 3 && document.getElementById(elem_id).value.length <= 10) {
                    //valid!!
                    valid = true;
                }
                else {
                    setElementFeedback(elem_id, "INVALID RANGE (3-10)", true);
                }

            }
            else {
                setElementFeedback(elem_id, "WRONG FORMAT FOR PROJECT-ID", true);
            }
            break;
        case "owner-name":
            if (document.getElementById(elem_id).value.replace(owner_regex, "").length <= 0) {
                if (document.getElementById(elem_id).value.length >= 3 && document.getElementById(elem_id).value.length <= 10) {
                    //valid!!
                    valid = true;
                }
                else {
                    setElementFeedback(elem_id, "INVALID RANGE (3-10)", true);
                }
            }
            else {
                setElementFeedback(elem_id, "WRONG FORMAT FOR OWNER NAME", true);
            }
            break;
        case "title":
            if (document.getElementById(elem_id).value.length >= 3 && document.getElementById(elem_id).value.length <= 25) {
                //valid!!
                valid = true;
            }
            else {
                setElementFeedback(elem_id, "INVALID RANGE (3-25)", true);
            }
            break;
        case "hours":
            if (document.getElementById(elem_id).value.replace(number_only_regex, "").length <= 0) {
                if (document.getElementById(elem_id).value >= 0 && document.getElementById(elem_id).value < 1000) {
                    //valid!!
                    valid = true;
                }
                else {
                    setElementFeedback(elem_id, "INVALID RANGE (0-999)", true);
                }

            }
            else {
                setElementFeedback(elem_id, "NUMBERS ONLY", true);

            }
            break;
        case "rate":
            if (document.getElementById(elem_id).value.replace(number_only_regex, "").length <= 0) {
                if (document.getElementById(elem_id).value >= 0 && document.getElementById(elem_id).value < 1000) {
                    //valid!!
                    valid = true;
                }
                else {
                    setElementFeedback(elem_id, "INVALID RANGE (0-999)", true);
                }

            }
            else {
                setElementFeedback(elem_id, "NUMBERS ONLY", true);

            }
            break;
        case "proj-desc":
            if (document.getElementById(elem_id).value.length >= 3 && document.getElementById(elem_id).value.length <= 65) {
                //valid!!
                valid = true
            }
            else {
                setElementFeedback(elem_id, "INVALID RANGE (3-65)", true);
            }
            break;


        default:
            console.error("validateElement was passed in a wrong element_id!");
            break;
    }

    if (valid) {
        //erase error message on that element
        setElementFeedback(elem_id, "", false);
    }
    return valid;
}

/** 
* Checks the validation of all the fields and sets the #add-button button accordingly.
* @author Brandon P. Olynyk
*/
function checkAllValidation() {

    let add_button = document.querySelector("#add-button");
    let image_array = document.querySelectorAll("img.input-image");

    let allValid = true;

    image_array.forEach(function(element)
    {
        if (element.src.endsWith("x.gif")) {
            allValid = false;          
        }
    })

   
    enable_disable_Button(add_button, !allValid);
}

/**
 * Creates a table from the ArrayObjects passed through.
 * @param objectArray 
 * @param parent The parent to append the created object to.
 * @author Brandon P. Olynyk
 */
function createTableFromArrayObjects(objectArray, parent) {
    //get variables from input values
    //remove existing table
    let existingTable = document.querySelector('#table');
    if (existingTable != null) {
        document.querySelector('#table-render-space').removeChild(existingTable);
    }

    //creating nodes for the table
    let table = document.createElement("table");

    table.id = "table";

    let tbody = document.createElement("tbody");


    //appending the header to the tbody. Don't do this if there's no objects
    if (objectArray[0] != null && objectArray[0] != undefined) {
        for (let i = 0; i < 8; i++) {
            //using the object key as the string to put in th
            try {
                let th = document.createElement("th");
                th.append(Object.keys(objectArray[0])[i]);
                tbody.appendChild(th);
            }
            catch (err) {
                console.error("Tried to get keys of an undefined object, skip: " + err);
            }

        }
    }

    let edit_th = document.createElement("th");
    edit_th.append("edit");
    tbody.appendChild(edit_th);

    let del_th = document.createElement("th");
    del_th.append("delete");
    tbody.appendChild(del_th);

    //repeat this block for each object inside array (one row for each object)
    for (let i = 0; i < objectArray.length; i++) {

        try {
            let tr = document.createElement("tr");
            tr.id = "table-row-" + i;
            
            //repeat this block for each column of info
            for (let j = 0; j < 8; j++) {
                let td = document.createElement("td");
                td.id = "table-cell-"+i+"-"+j;
                td.className = "table-cell-in-row-"+i;
                //appending the values to the nodes
                td.appendChild(document.createTextNode(Object.values(objectArray[i])[j]));
                tr.appendChild(td);

            }
            //adding the columns for edit and trash icons

            let edit_node = document.createElement("td");
            let edit_img = document.createElement("img");
            edit_img.src = '../images/edit.png';
            //add id and event to img
            edit_img.id = "edit-img-" + i;
            edit_node.addEventListener("mousedown", editTableRow)
            edit_node.appendChild(edit_img);
            tr.appendChild(edit_node);

            let del_node = document.createElement("td");
            let del_img = document.createElement("img");
            del_img.src = '../images/trash.png';
            //add and id to trash img, and event
            
            del_img.id = "bin-img-" + i;
            del_img.addEventListener("click", deleteTableRow)

            del_node.appendChild(del_img);
            tr.appendChild(del_node);

            tbody.appendChild(tr);
        }
        catch (err) {
            console.error("ObjectArray is null! Skipping drawing of that entry: " + err);
        }
    }
    table.appendChild(tbody);
    document.querySelector('#table-render-space').appendChild(table);


}

/** 
* Resets all the fields of the inputs and checks all field validations
* @author Brandon P. Olynyk
*/
function resetInputFields() {
    document.querySelectorAll("input").forEach(element => element.value = "");
    checkAllValidation();
}