'use strict'

/**
 * regex form validation
 * @author Tony Nguyen
 */
let proj_id_regex = /^[a-zA-Z]\w*\$*/;
let owner_regex = /^[a-zA-Z]\w*/;
let number_only_regex = /^[0-9]*$/;

let project_array = new Array(); //empty array that holds all the projects

//field elements (set when dom content loaded)
let e_proj_id = null;
let e_owner_name = null;
let e_title = null;
let e_hours = null;
let e_rate = null;
let e_proj_desc = null;
let e_category = null;
let e_status = null;

//the type of sorting
let sort_type = 0;


window.addEventListener('DOMContentLoaded', function () {

   InitializeInputElements();

   document.getElementById("add-button").addEventListener("click", updateProjectsTable);

   document.getElementById("reset-button").addEventListener("click", resetInputFields);

   document.getElementById("search-bar").addEventListener("change", filterProjects);


   hideElementFeedback();
   checkAllValidation();

   enable_disable_Button(document.getElementById("add-button"), true);  //disable button on startup
})

/**
 * Initializes all the input elements as well as sets them to required.
 * @author Brandon P. Olynyk
 */
function InitializeInputElements() {
   e_proj_id = document.getElementById("proj-id");
   e_owner_name = document.getElementById("owner-name");
   e_title = document.getElementById("title");
   e_hours = document.getElementById("hours");
   e_rate = document.getElementById("rate");
   e_proj_desc = document.getElementById("proj-desc");
   e_category = document.getElementById("category");
   e_status = document.getElementById("status");

   //we want to validate each element at the start too
   e_proj_id.addEventListener("blur", validateElement);
   e_owner_name.addEventListener("blur", validateElement);
   e_title.addEventListener("blur", validateElement);
   e_hours.addEventListener("blur", validateElement);
   e_rate.addEventListener("blur", validateElement);
   e_proj_desc.addEventListener("blur", validateElement);

   set_elem_required("proj-id", true);
   set_elem_required("owner-name", true);
   set_elem_required("title", true);
   set_elem_required("hours", true);
   set_elem_required("rate", true);
   set_elem_required("proj-desc", true);
   set_elem_required("category", true);
   set_elem_required("status", true);

   document.getElementById("save-local").addEventListener("click", saveAllProjectsToStorage);
   document.getElementById("append-local").addEventListener("click", appendAllProjectsToStorage);
   document.getElementById("clear-local").addEventListener("click", clearAllProjectsToStorage);
   document.getElementById("load-local").addEventListener("click", readAllProjectsToStorage);

   document.getElementById("sort-type-button").addEventListener("click", sortProjects);
}


