# f22-proj1-grp9-OLYNYK

This is a project management system by Brandon P. Olynyk and Tony Nguyen.

# DESCRIPTION
This program is able to keep info about projects. Once you add the project, the program will list them in a table. You can then search by entering text in the searchbar and clicking out of it, as well as edit and delete projects by clicking on their icon in the table.

To add a project to the table, enter the relevant info in the top input fields, then click the add button once all the fields are valid.

Use the Reset buttons to reset the fields you have written already in the input fields. 

Use the save buttons to save your projects to local storage. In order, these buttons:
- Saves all the project data into local storage and erases the pre-existing data
- Appends the current project data into the already existing data
- Clears all saved project data
- Loads saved project data into the table

Click the sort button to change the property of the projects to be sorted.


